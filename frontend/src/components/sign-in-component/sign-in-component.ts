import {Component} from "@angular/core";
import {NavController, NavParams} from "ionic-angular";
import {AuthController} from "../../controllers/auth-controller";
import {HomePage} from "../../pages/home/home";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {EmailValidatorDirective} from "../email-validator-directive/email-validator-directive";

/**
 * Generated class for the SignInComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'sign-in-component',
  templateUrl: 'sign-in-component.html'
})
export class SignInComponent {

  usercreds: { email: string, password: string } = { email: null, password: null };
  message: { status: 'success' | 'danger', msg: string};
  signinForm: FormGroup;
  submitAttempt: boolean;
  private emailPattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private authCtrl: AuthController,
              private formBuilder: FormBuilder
  ) {
    this.setSignInForm();
  }

  submitLoginForm(data: any) {
    this.message = null;
    if (this.signinForm.valid ) {
      this.authCtrl.login(this.usercreds)
        .then((resp) => {
          this.message = { status: 'success', msg: 'You have been logged' };
          this.navCtrl.setRoot(HomePage);
        })
        .catch((error) => {
          this.message = { status: 'danger', msg: error.message }
        });
    } else {
      this.submitAttempt = true;
    }
  }

  private setSignInForm() {
    this.signinForm = this.formBuilder.group({
      'email': [
        '',
        Validators.compose([Validators.required, Validators.pattern(this.emailPattern)])
      ],
      'password': [
        '',
        Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(42)])
      ],
    })
  }

  isInValid(prop: string): boolean {
    return !this.signinForm.controls[prop].valid  && (this.signinForm.controls[prop].dirty || this.submitAttempt)
  }
}
