import {BrowserModule} from "@angular/platform-browser";
import {ErrorHandler, NgModule} from "@angular/core";
import {IonicApp, IonicErrorHandler, IonicModule} from "ionic-angular";
import {SplashScreen} from "@ionic-native/splash-screen";
import {StatusBar} from "@ionic-native/status-bar";

import {MyApp} from "./app.component";
import {HomePage} from "../pages/home/home";
import {DashboardComponent} from "../components/dashboard-component/dashboard-component";
import {SolarPanelComponent} from "../components/solar-panel-component/solar-panel-component";
import {SolarPanelDetailsPage} from "../pages/solar-panel-details/solar-panel-details";
import {SolarPanelProvider} from "../providers/solar-panel-provider";
import {AuthProvider} from "../providers/auth-provider";
import {HttpModule} from "@angular/http";
import {LoggingPage} from "../pages/logging/logging";
import {HistoryPage} from "../pages/history/history";
import {PlanificationPage} from "../pages/planification/planification";
import {AuthController} from "../controllers/auth-controller";
import {SignUpComponent} from "../components/sign-up-component/sign-up-component";
import {SignInComponent} from "../components/sign-in-component/sign-in-component";
import {StatChartComponent} from "../components/stat-chart-component/stat-chart-component";
import "../../node_modules/chart.js/dist/Chart.bundle.min.js";
import {ChartsModule} from "ng2-charts";
import {PrevisionCalendarComponent} from "../components/prevision-calendar-component/prevision-calendar-component";
import { CalendarModule } from 'angular-calendar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MultiPickerModule } from 'ion2-datetime-picker';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    DashboardComponent,
    SolarPanelComponent,
    LoggingPage,
    SolarPanelDetailsPage,
    SignUpComponent,
    SignInComponent,
    StatChartComponent,
    PlanificationPage,
    HistoryPage,
    PrevisionCalendarComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    ChartsModule,
    CalendarModule.forRoot(),
    BrowserAnimationsModule,
    MultiPickerModule, //Import MultiPickerModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    SolarPanelDetailsPage,
    PlanificationPage,
    LoggingPage,
    HistoryPage,
    SignUpComponent,
    SignInComponent,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    SolarPanelProvider,
    AuthProvider,
    AuthController,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
