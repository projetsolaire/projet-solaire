import {Component} from "@angular/core";
import {NavController, NavParams} from "ionic-angular";
import {LoggingPage} from "../logging/logging";
import {AuthController} from "../../controllers/auth-controller";
import {UserModel} from "../../models/UserModel";
import config from "../../config";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  user: UserModel;

  constructor(public navCtrl: NavController, private navParams: NavParams, private authCtrl: AuthController) {
    if (this.authCtrl.isLogin()) {
      this.user = this.authCtrl.getUser();
    } else {
      this.navCtrl.setRoot(LoggingPage);
    }
  }

}
