import {Component} from "@angular/core";
import {SolarPanelModel} from "../../models/SolarPanelModel";
import {SolarPanelProvider} from "../../providers/solar-panel-provider";
import {AlertController, NavController} from "ionic-angular";
import {LoggingPage} from "../../pages/logging/logging";

@Component({
  selector: 'dashboard-component',
  templateUrl: 'dashboard-component.html'
})
export class DashboardComponent {

  solarPanels: SolarPanelModel[] = [];
  error: string;

  constructor(private navCtrl: NavController, private alertCtrl: AlertController, private panelProvider: SolarPanelProvider) {
    this.panelProvider.getAll().subscribe(
      (res) => {
        res.forEach(
          (panel) => this.solarPanels.push(new SolarPanelModel(panel.id, panel.name, panel.state))
        );
      },
      (error) => {
        this.navCtrl.setRoot(LoggingPage);
      }
    );
  }

  tapSolarPanel(evt: Event) {
    evt.target
  }

  presentRegisterPrompt() {
    this.error = null;
    const self = this;
    let alert = this.alertCtrl.create({
      title: 'Register Panel',
      inputs: [
        {
          name: 'reference',
          placeholder: 'reference'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Register',
          handler: data => {
            self.register(data.reference);
          }
        }
      ]
    });
    alert.present();
  }

  register(ref: string) {
    this.error = null;
    this.panelProvider.register(ref).subscribe(
      (res) => {
        this.solarPanels.push(new SolarPanelModel(res.id, res.name, res.state));
      },
      (err) => {
        console.log(err.error);
        if (err.error.statusCode === 409) {
          this.error = err.error.message;
        }
      }
    );
  }

}
