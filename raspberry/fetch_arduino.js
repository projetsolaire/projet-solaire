var SerialPort = require('serialport');
const request = require('request');
const http = require('http');
const url = require('url');
const average = arr => arr.reduce( ( p, c ) => p + c, 0 ) / arr.length;

const hostNameDistant = '172.20.10.2';
const portServerLocal = 8080;
const portServerDistant = 3000;
const pathToPushData = '/api/panels/3/data';
const pathToSubscribe = '/api/panels/3/subscribe';

var initValue = false; //push une valeur sur un "on"

var nbValuestoPush = 100;
var tempIn = [];
var tempOut = [];
var isFetching = "0";

var objectToPush =
    {
        meanIn  : 0,
        meanOut : 0
    };


request.patch({
    url: "http://" + hostNameDistant + ":" + portServerDistant + pathToSubscribe,
}, function (err, resp, body) {
    if ( err ) {
        console.log("ERROR FROM BACK : ", err);
    }
    if (!err && resp.statusCode == 200) {
        console.log(body);
        var port = new SerialPort('/dev/ttyACM0',
            {
                parser: SerialPort.parsers.readline()
            },
            function (err) {
                if (err) {
                    return console.log('Error: ', err.message);
                }

                port.on('data', function (data) {


                    data = data.split(";");
                    console.log(data[0]);
                    console.log(data[1]);

                    if (tempIn.length  < nbValuestoPush) {

                        //on push les donnees dans deux tableaux differents
                        tempIn.push(parseFloat(data[0]));
                        tempOut.push(parseFloat(data[1]));

                        if ( initValue ) {

                            objectToPush.meanIn  = (Math.round(average(tempIn)*100)/100);
                            objectToPush.meanOut = (Math.round(average(tempOut)*100)/100);

                            console.log("MEANS : ", objectToPush.meanIn, " : ", objectToPush.meanOut);

                            request.post({
                                url: "http://" + hostNameDistant + ":" + portServerDistant + pathToPushData,
                                json: objectToPush
                            }, function (err, resp, body) {
                                if ( err ) {
                                    console.log("ERROR FROM BACK : ", err);
                                }
                                if (!err && resp.statusCode == 200) {
                                    console.log(body);
                                }
                            });

                            initValue = false;
                        }

                    }

                    else {
                        objectToPush.meanIn  = (Math.round(average(tempIn)*100)/100);
                        objectToPush.meanOut = (Math.round(average(tempOut)*100)/100);

                        console.log("MEANS : ", objectToPush.meanIn, " : ", objectToPush.meanOut);

                        request.post({
                            url: "http://" + hostNameDistant + ":" + portServerDistant + pathToPushData,
                            json: objectToPush
                        }, function (err, resp, body) {
                            if ( err ) {
                                console.log("ERROR FROM BACK : ", err);
                            }
                            if (!err && resp.statusCode == 200) {
                                console.log(body);
                            }
                        });
                        tempIn  = [];
                        tempOut = [];
                    }
                });

            });

        const serverLocal = http.createServer(function (req, res) {
            req.on("error", function (err) {
                res.statusCode = 400;
                console.log(error);
            });
            res.on("error", function (err) {
                res.statusCode = 500;
                console.log(error);
            });
            if(req.method === "PATCH") {

                var url_parts = url.parse(req.url);

                if (req.url === "/on") {
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'text/plain');
                    res.write("{\"state\" : \"on\"}");
                    isFetching = "~"; //CODE ASCII = 126
                    console.log("fetch en cours");
                    port.write(isFetching);
                    initValue = true;
                }


                if (req.url === "/off") {
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'text/plain');
                    res.write("{\"state\" : \"off\"}");
                    isFetching = "e"; //CODE ASCII = 101
                    console.log("fetch stoppé");
                    port.write(isFetching);
                }

                if (url_parts.pathname === "/speed") {
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'text/plain');
                    //le true permet de parser en renvoyant object json et on recup speed direct
                    var speed = url.parse(req.url, true).query.speed;
                    console.log(speed);
                    /*if (speed < 0)
                     {
                     speed = 0;
                     } if (speed > 100)
                     {
                     speed = 100;
                     }*/
                    res.write("{\"speed\" : " + speed + "}");
                    console.log("changement de vitesse : ", speed, "%");
                    port.write(String.fromCharCode(speed));

                }

            }

            res.end();

        });
        serverLocal.listen(portServerLocal);
    }
});



