import {Component, ViewChild} from "@angular/core";
import {MenuController, Nav, Platform} from "ionic-angular";
import {StatusBar} from "@ionic-native/status-bar";
import {SplashScreen} from "@ionic-native/splash-screen";

import {HomePage} from "../pages/home/home";
import {LoggingPage} from "../pages/logging/logging";
import {AuthController} from "../controllers/auth-controller";
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage:any = HomePage;

  constructor(platform: Platform, public menu: MenuController, private authCtrl: AuthController, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      // TODO window['plugins'].googleplus.trySilentLogin(...); @see cordova-plugin-googleplus
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }

  logout() {
    this.authCtrl.logout();
    this.nav.setRoot(LoggingPage);
    this.menu.close();
  }
}

