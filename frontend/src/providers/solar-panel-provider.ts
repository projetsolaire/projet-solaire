import {Injectable} from "@angular/core";
import {Http, RequestOptions} from "@angular/http";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
import {Observable} from "rxjs/Observable";
import {BasicProvider} from "./basic-provider";
import {AuthController} from "../controllers/auth-controller";
import {UserModel} from "../models/UserModel";

/*
  Generated class for the SolarPanelProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class SolarPanelProvider extends BasicProvider {

  constructor(public http: Http, public authCtrl: AuthController) { super(); }

  switch(panelId: string, newState: string): Observable<any> {
    return this.http.patch(this.apiUrl + '/panels/' + panelId + '/switch/' + newState, {}, { headers: this.getAuthHeader() })
                    .map(this.extractData)
                    .catch(this.handleError);
  }

  patchSpeed(panelId: string, value: number): Observable<any> {
    const options = new RequestOptions({ headers: this.getAuthHeader() });

    return this.http.patch(this.apiUrl + '/panels/' + panelId + '/speed', { speed: value }, options)
                    .map(this.extractData)
                    .catch(this.handleError);
  }

  register(ref: string) {
    const options = new RequestOptions({ headers: this.getAuthHeader() });
    const user: UserModel = this.authCtrl.getUser();
    console.log(ref);
    return this.http.patch(this.apiUrl + '/panels/' + ref + '/associate', { userId: user.id+"" }, options)
                    .map(this.extractData)
                    .catch(this.handleError);
  }

  rename(panelId: string, newName: string) {
    const options = new RequestOptions({ headers: this.getAuthHeader() });

    return this.http.patch(this.apiUrl + '/panels/' + panelId + '/edit', { name: newName }, options)
                    .map(this.extractData)
                    .catch(this.handleError);
  }

  getAll(): Observable<any> {
    let options = new RequestOptions({ headers: this.getAuthHeader() });
    const user: UserModel = this.authCtrl.getUser();

    return this.http.get(this.apiUrl + '/users/'+ user.id + '/panels', options)
               .map(this.extractData)
               .catch(this.handleError);
  }

  get(panelId: string): Observable<any> {
    let options = new RequestOptions({ headers: this.getAuthHeader() });
    const user: UserModel = this.authCtrl.getUser();
    return this.http.get(this.apiUrl + '/users/' + user.id + '/panels/' + panelId, options)
                    .map(this.extractData)
                    .catch(this.handleError);
  }

  getStatsYear(panelId: string, year: string) : Observable<any> {
    let options = new RequestOptions({ headers: this.getAuthHeader() });
    return this.http.get(this.apiUrl + '/panels/' + panelId + '/data/year/'  + year, options)
      .map(this.extractData)
      .catch(this.handleError);
  }

  getStatsMonth(panelId: string,year:string, month: string) : Observable<any> {
    let options = new RequestOptions({ headers: this.getAuthHeader() });
    return this.http.get(this.apiUrl + '/panels/' + panelId + '/data/month' + '/' + year + '/' + month, options)
      .map(this.extractData)
      .catch(this.handleError);
  }

  getStatsDay(panelId: string, year:string, month:string, day:string) : Observable<any> {
    let options = new RequestOptions({ headers: this.getAuthHeader() });
    return this.http.get(this.apiUrl + '/panels/' + panelId + '/data/day' + '/' + year + '/' + month + '/' + day, options)
      .map(this.extractData)
      .catch(this.handleError);
  }

  getEvents(panelId: string ):Observable<any>{
    const user: UserModel = this.authCtrl.getUser();
    let options = new RequestOptions({ headers: this.getAuthHeader() });
    return this.http.get(this.apiUrl + '/users/' +user.id+'/panels/'+ panelId + '/events', options)
      .map(this.extractData)
      .catch(this.handleError);
  }

  postEvent(panelId: string, debut:Date, fin:Date, title:string, state:string):Observable<any>{
    const user: UserModel = this.authCtrl.getUser();
    let options = new RequestOptions({ headers: this.getAuthHeader() });
    let body = {start:debut, end:fin, state:state, label:title};
    return this.http.post(this.apiUrl + '/users/' +user.id+'/panels/'+ panelId + '/events', body, options)
      .map(this.extractData)
      .catch(this.handleError);
  }

  deleteEvent(panelId: string, eventId):Observable<any>{
    const user: UserModel = this.authCtrl.getUser();
    let options = new RequestOptions({ headers: this.getAuthHeader() });
    return this.http.delete(this.apiUrl + '/users/' +user.id+'/panels/'+ panelId + '/events/' +eventId, options)
      .map(this.extractData)
      .catch(this.handleError);
  }

}
