
export class SolarPanelModel {

  public isOn: boolean;
  constructor(public id: string,
              public name: string,
              public state: string,
              public meansTemp: {inValue: number, outValue: number, date: string}[] = [],
              public temperature: number = 0,
              public speed: number = 0
  ) {
    this.isOn = state == 'on';
  }

  toogleState() {
    this.isOn = !this.isOn;
  }

  getState(): string {
    return this.isOn ? 'on' : 'off';
  }

  set(data: any) {
    this.id = data.id;
    this.name = data.name;
    this.state = data.state;
    this.meansTemp = data.data.means;
    this.temperature = this.meansTemp && this.meansTemp.length != 0 ? this.meansTemp[this.meansTemp.length-1].outValue : null;
    console.log(data.speed);
    this.speed = data.speed ? data.speed : 0;
  }
}
