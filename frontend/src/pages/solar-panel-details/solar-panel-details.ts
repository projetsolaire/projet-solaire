import { Component } from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {SolarPanelModel} from "../../models/SolarPanelModel";
import {SolarPanelProvider} from "../../providers/solar-panel-provider";
import {HistoryPage} from "../../pages/history/history"
import {PlanificationPage} from "../planification/planification";

declare var $: any;

/**
 * Generated class for the SolarPanelDetails page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-solar-panel-details',
  templateUrl: 'solar-panel-details.html',
})
export class SolarPanelDetailsPage {
  solarPanel: SolarPanelModel;
  slider : any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private alertCtrl: AlertController,
              private panelProvider: SolarPanelProvider
  ) {
    this.solarPanel = this.navParams.get('solarPanel');
    this.panelProvider.get(this.solarPanel.id).subscribe(
      (res) => {
        this.solarPanel.set(res) // set all properties of a solarPanel
        this.slider.roundSlider('setValue', res.speed);
      }
    );
  }
  tooglePanelState(evt) {
    const newState: string = this.solarPanel.isOn ? 'off' : 'on';
    this.panelProvider.switch(this.solarPanel.id, newState).subscribe(
      (res) => {
        let toogleOnOk = !this.solarPanel.isOn && res.state === 'on';
        let toogleOffOk = this.solarPanel.isOn && res.state === 'off';
        if (toogleOnOk || toogleOffOk) {
          this.solarPanel.toogleState();
        } else {
          // error
        }
      }
    )
  }

  tempChanged(value: number) {
    this.panelProvider.patchSpeed(this.solarPanel.id, value).subscribe(
      (res) => {
        console.log(res);
      },
      (err) => console.log(err)
    );
  }

  presentConfirmSpeedAlert(speedValue: number, previousSpeedValue: number, slider: any) {
    let self = this;
    let alert = this.alertCtrl.create({
      title: 'Confirm speed',
      message: 'Do you want to set the speed to ' + speedValue + '% ?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            slider.setValue(previousSpeedValue);
          }
        },
        {
          text: 'Confirm',
          handler: () => {
            self.tempChanged(speedValue);
          }
        }
      ]
    });
    alert.present();
  }

  ionViewDidLoad() {
    const self = this;
    const options = {
      'circleShape': 'half-top',
      'handleShape': 'dot',
      min: 0,
      max: 100,
      value: this.solarPanel.speed
    };
    this.slider = $('#slider-temp');
    this.slider.roundSlider(options);

    this.slider.on('stop', function(e) {
      self.presentConfirmSpeedAlert(e.value, self.solarPanel.speed, self.slider.data('roundSlider'));
    });
  }

  disableSlider() {
    this.slider.roundSlider('disable');
  }

  tapHistoryPage() {
    this.navCtrl.push(HistoryPage, { panel: this.solarPanel });
  }

  tapPlanificationPage(){
    this.navCtrl.push(PlanificationPage, { panel: this.solarPanel });
  }


}
