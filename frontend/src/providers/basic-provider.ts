import {Headers, Response} from "@angular/http";
import config from "../config.ts";
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/throw';

export abstract class BasicProvider {

  protected apiUrl: string = config.apiUrl;
  constructor() { }

  getAuthHeader(): Headers {
    const headers = new Headers();
    // TODO getToken() in authController
    const user = JSON.parse(window.localStorage.getItem(config.userStore));
    console.log(window.localStorage.getItem(config.userStore));
    headers.append('Authorization', user.id);
    return headers;
  }

  handleError(error: Response | any) {
    let resError: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      resError = error.json() || JSON.stringify(body);
    } else {
      resError = error.message ? error.message : error.toString();
    }
    return Observable.throw(resError);
  }

  extractData(res: Response) {
    let body = res.json();
    console.log(body);
    return body;
  }
}
