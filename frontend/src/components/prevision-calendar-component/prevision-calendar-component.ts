import { Component, Input, OnInit } from '@angular/core';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder } from '@angular/forms';
import moment from 'moment';
import {SolarPanelProvider} from "../../providers/solar-panel-provider";
import {SolarPanelModel} from "../../models/SolarPanelModel";
import { AlertController } from 'ionic-angular';




/**
 * Generated class for the PrevisionCalendarComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'prevision-calendar-component',
  templateUrl: 'prevision-calendar-component.html'
})
export class PrevisionCalendarComponent implements OnInit{

  @Input() panel: SolarPanelModel;

  public currentDate = new Date(Date.now());
  private toggleAddEvent_m:boolean = false;
  openActiveDay:boolean=false;
  stateEvent = "on";
  mapEventId : Map<string,number> = new Map<string,number>();


  days_label: Array<string> = [
    'Sun', 'Mon', 'Tue', 'Wed', 'Thur', 'Fri', 'Sat'
  ];

  public colors: any = {
    red: {
      primary: '#ad2121',
      secondary: '#FAE3E3'
    },
    blue: {
      primary: '#1e90ff',
      secondary: '#D1E8FF'
    },
    yellow: {
      primary: '#e3bc08',
      secondary: '#FDF1BA'
    },
    green: {
      primary: '#46e30f',
      secondary: '#bdfdac'
    }
  };

  public calEvents: CalendarEvent[] = [];

  public debEvent = new Date(Date.now()).toISOString();

  private formGroupDeb;
  private formGroupFin;



  constructor(private panelProvider: SolarPanelProvider, public alertCtrl: AlertController) {
    this.formGroupDeb = new FormBuilder().group({
      time: [moment().format()],
    });
    this.formGroupFin = new FormBuilder().group({
      time: [moment().format()]
    });


  }

  ngOnInit(){
     this.panelProvider.getEvents(this.panel.id).subscribe(
       (res) => {this.setAllEvents(res)}
     );
  }

  addEvent() {
    if (this.formGroupDeb.controls.time.value <= this.formGroupFin.controls.time.value) {
      let dateDeb =new Date(this.formGroupDeb.controls.time.value);
      let dateFin =new Date(this.formGroupFin.controls.time.value);
      let newTitle:string = dateDeb.getHours()+":"+dateDeb.getMinutes()+" - "+dateFin.getHours()+":"+dateFin.getMinutes();
      // let event:CalendarEvent={
      //   start: this.currentDate,
      //   title: newTitle,
      //   color: this.colors.blue}
      // this.calEvents.push(event);
      // this.calEvents = this.calEvents.slice();

      this.postEvent(dateDeb, dateFin, this.currentDate, newTitle);
      this.toggleAddEvent_m = !this.toggleAddEvent_m;
  }
  }

  toggleAddEvent(){
    this.toggleAddEvent_m = !this.toggleAddEvent_m;
  }

  changeCurrentDay(day){

    let regexDay = new RegExp(/^([0-9]{1,2})$/);

    if(day.target.innerText.match(regexDay)) {
      console.log(day);
      if (<number>(day.target.innerText) == this.currentDate.getDate()) {
        this.openActiveDay = !this.openActiveDay;
      }
      else {
        this.currentDate = new Date(this.currentDate.getFullYear(), this.currentDate.getMonth(), day.target.innerText, 0, 0, 0, 0);
        this.toggleAddEvent_m = false;
        this.openActiveDay = true;
      }
    }
  }

  postEvent(deb:Date, fin:Date, day:Date, title:string){
    let dateDeb =new Date(day.getFullYear(), day.getMonth(), day.getDate(), deb.getHours(), deb.getMinutes());
    let dateFin =new Date(day.getFullYear(), day.getMonth(), day.getDate(), fin.getHours(), fin.getMinutes());

    this.panelProvider.postEvent(this.panel.id, dateDeb, dateFin, title, this.stateEvent).subscribe(
      (res) => {this.setNewEvent(res)}
      );
  }



  showMonth(month:number){
    switch (month){
      case 0: return 'Janvier';
      case 1: return 'Février';
      case 2: return 'Mars';
      case 3: return 'Avril';
      case 4: return 'Mai';
      case 5: return 'Juin';
      case 6: return 'Juillet';
      case 7: return 'Août';
      case 8: return 'Septembre';
      case 9: return 'Octobre';
      case 10: return 'Novembre';
      case 11: return 'Décembre';
    }
  }

  nextMonth(){
    if(this.currentDate.getMonth() == 11) {
      this.currentDate = new Date(this.currentDate.getFullYear()+1, 0, 1, 0, 0, 0, 0);
    }
    else{
      this.currentDate = new Date(this.currentDate.getFullYear(), this.currentDate.getMonth() + 1, 1, 0, 0, 0, 0);

    }
  }
  previousMonth(){
    if(this.currentDate.getMonth() == 0) {
      this.currentDate = new Date(this.currentDate.getFullYear()-1, 11, 1, 0, 0, 0, 0);
    }
    else {
      this.currentDate = new Date(this.currentDate.getFullYear(), this.currentDate.getMonth()-1, 1, 0, 0, 0, 0);
    }
  }

  setAllEvents(res){
    for (let event of res){
      let color = this.colors.blue;

      if (event.state == "on"){
        color = this.colors.green;
      }
      if (event.state == "off"){
        color = this.colors.red;
      }

      let newEvent:CalendarEvent={
        start: new Date(event.start),
        title: event.label,
        color: color}
      this.calEvents.push(newEvent);
      this.mapEventId[event.label] = event.id;
      this.calEvents = this.calEvents.slice();
    }
  }

  setNewEvent(event){
      let color = this.colors.blue;

      if (event.state == "on"){
        color = this.colors.green;
      }
      if (event.state == "off"){
        color = this.colors.red;
      }

      let newEvent:CalendarEvent={
        start: new Date(event.start),
        title: event.label,
        color: color}
      this.calEvents.push(newEvent);
      this.mapEventId[event.label] = event.id;
      this.calEvents = this.calEvents.slice();
  }

  showConfirm(e) {
    let confirm = this.alertCtrl.create({
      title: 'Delete Event',
      message: 'Do you want to delete the event : '+e.event.title,
      buttons: [
        {
          text: 'Disagree',
          handler: () => {
            return;
          }
        },
        {
          text: 'Agree',
          handler: () => {
            this.deleteEvent(e.event.title);
          }
        }
      ]
    });
    confirm.present();
}

  deleteEvent(title:string){
  let eventId = this.mapEventId[title];
  this.panelProvider.deleteEvent(this.panel.id, eventId).subscribe();
  //non supprime de la map
    for (let events of this.calEvents){
      if(events.title === title){
        var index = this.calEvents.indexOf(events, 0);
        if (index > -1) {
          this.calEvents.splice(index, 1);
          this.calEvents = this.calEvents.slice();
        }
      }
    }
}

}
