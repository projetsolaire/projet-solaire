import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SignUpComponent } from './sign-up-component';

@NgModule({
  declarations: [
    SignUpComponent,
  ],
  imports: [
    IonicPageModule.forChild(SignUpComponent),
  ],
  exports: [
    SignUpComponent
  ]
})
export class SignUpComponentModule {}
