import { Component } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthController} from "../../controllers/auth-controller";
import {HomePage} from "../../pages/home/home";
import {NavController} from "ionic-angular";

/**
 * Generated class for the SignUpComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'sign-up-component',
  templateUrl: 'sign-up-component.html'
})
export class SignUpComponent {

  user: { email: string,
          username: string,
          password: string,
          passwordRepeat: string
        };
  message: { status: 'SUCCESS' | 'ERROR', msg: string};
  signupForm: FormGroup;
  submitAttempt: boolean = false;
  private emailPattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;

  constructor(private navCtrl: NavController, private authCtrl: AuthController, private formBuilder: FormBuilder) {
    this.user = { email: null,
                  password: null,
                  username: null,
                  passwordRepeat: null
    };
    this.setSignUpForm();
  }

  submitSignUpForm() {
    if (this.signupForm.valid) {
      let userUp = { email: this.user.email, password: this.user.password, username: this.user.username };
      this.authCtrl.signup(userUp).then(
        (resp) => {
          this.authCtrl.login({ email: this.user.email, password: this.user.password }).then(
            (resp) => {
              this.message = { status: 'SUCCESS', msg: 'You have been logged' };
              this.navCtrl.setRoot(HomePage);
            }
          );
        }
      ).catch((error) => this.message = {status: 'ERROR', msg: error.message});

    } else {
      this.submitAttempt = true;
    }
  }

  private setSignUpForm() {
    this.signupForm = this.formBuilder.group({
      'email': [
        '',
        Validators.compose([Validators.required, Validators.pattern(this.emailPattern)])
      ],
      'username': [
        '',
        Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(42)])
      ],
      'passwords': this.formBuilder.group( {
        'password': [
          '',
          Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(42)])
        ],
        'repeat': [
          '',
          Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(42)])
        ]
      }, { validator: this.areEqual }
      )
    })
  }

  isInValid(prop: string): boolean {
    return !this.signupForm.controls[prop].valid  && (this.signupForm.controls[prop].dirty || this.submitAttempt);
  }

  areEqual(group: any): any {
    let valid = true;

    let firstVal: string = null;
    for (let name in group.controls) {
      if (!firstVal) {
        firstVal = group.controls[name].value;
      } else {
        if (firstVal !== group.controls[name].value) {
          return {
            areEqual: false
          };
        }
      }
    }

    return null;
  }
  S
}
