var launchEventsJobs = require('./launchEventsJobs');
var server = null;
var logger = require('../logger');

function createEvent(panel, startDate, endDate){
  if(!endDate){
    endDate = null;
  }

  panel.events.create({
    start : startDate,
    end : endDate,
    state : 'on',
    label : 'EVENT 1'
  }, function(err, evt){
    if(err) return logger.error(JSON.stringify(err));
    logger.info("Event created : "+JSON.stringify(evt));
    //launchEventsJobs(server);
  });
}

function createPanel(user, data, address, cb){
  user.panels.create({
    address : address,
    data : data
  }, function(err, panel){
    if(err) throw err;
    logger.info("Panel created : "+JSON.stringify(panel));
    if(cb){
      cb(panel);
    }

  });
}

module.exports = function(app){
  server = app;
  app.models.user.create([{
    username : "test",
    email : "test@test.fr",
    password : "9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08"
  },
  {
    username : "test2",
    email : "test2@test.fr",
    password : "test2"
  },
  {
    username : "test3",
    email : "test3@test.fr",
    password : "9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08"
  }
], function(err, users){

  if(err) throw err;
  logger.info("User created : "+JSON.stringify(users));

  let address = "localhost";
  // let address = "172.20.10.768:8080";
  let data = { means : [
    {"inValue":25.35,"outValue":28.10,"date":"Sun Jan 1 2017 01:10:39 GMT+0200 (CEST)"},
    {"inValue":20.72,"outValue":24.72,"date":"Sun Jan 1 2017 02:10:39 GMT+0200 (CEST)"},
    {"inValue":20.22,"outValue":23.00,"date":"Sun Jan 1 2017 03:10:39 GMT+0200 (CEST)"},
    {"inValue":18.72,"outValue":20.00,"date":"Sun Jan 1 2017 04:10:39 GMT+0200 (CEST)"},
    {"inValue":23.72,"outValue":25.00,"date":"Sun Jan 1 2017 05:10:39 GMT+0200 (CEST)"},
    {"inValue":19.66,"outValue":20.00,"date":"Sun Jan 1 2017 06:10:39 GMT+0200 (CEST)"},
    {"inValue":21.72,"outValue":28.15,"date":"Sun Jan 1 2017 07:10:39 GMT+0200 (CEST)"},
    {"inValue":20.72,"outValue":20.80,"date":"Sun Jan 1 2017 08:10:39 GMT+0200 (CEST)"},
    {"inValue":20.22,"outValue":23.00,"date":"Sun Jan 1 2017 09:10:39 GMT+0200 (CEST)"},
    {"inValue":22.82,"outValue":33.50,"date":"Sun Jan 1 2017 10:10:39 GMT+0200 (CEST)"},
    {"inValue":20.72,"outValue":24.72,"date":"Sun Jan 1 2017 11:10:39 GMT+0200 (CEST)"},
    {"inValue":20.22,"outValue":23.00,"date":"Sun Jan 1 2017 12:10:39 GMT+0200 (CEST)"},
    {"inValue":20.22,"outValue":23.00,"date":"Sun Jan 1 2017 13:10:39 GMT+0200 (CEST)"},
    {"inValue":18.72,"outValue":20.00,"date":"Sun Jan 1 2017 14:10:39 GMT+0200 (CEST)"},
    {"inValue":23.72,"outValue":25.00,"date":"Sun Jan 1 2017 15:10:39 GMT+0200 (CEST)"},
    {"inValue":19.66,"outValue":20.00,"date":"Sun Jan 1 2017 16:10:39 GMT+0200 (CEST)"},
    {"inValue":21.72,"outValue":28.15,"date":"Sun Jan 1 2017 17:10:39 GMT+0200 (CEST)"},
    {"inValue":20.72,"outValue":20.80,"date":"Sun Jan 1 2017 18:10:39 GMT+0200 (CEST)"},
    {"inValue":20.22,"outValue":23.00,"date":"Sun Jan 1 2017 19:10:39 GMT+0200 (CEST)"},
    {"inValue":22.82,"outValue":33.50,"date":"Sun Jan 1 2017 20:10:39 GMT+0200 (CEST)"},
    {"inValue":20.72,"outValue":24.72,"date":"Sun Jan 1 2017 21:10:39 GMT+0200 (CEST)"},
    {"inValue":20.22,"outValue":23.00,"date":"Sun Jan 1 2017 22:10:39 GMT+0200 (CEST)"},
    {"inValue":20.22,"outValue":23.00,"date":"Sun Jan 1 2017 23:10:39 GMT+0200 (CEST)"},
    {"inValue":18.72,"outValue":20.00,"date":"Sun Jan 1 2017 00:10:39 GMT+0200 (CEST)"},

    {"inValue":22.82,"outValue":15.50,"date":"Sun Feb 19 2017 17:10:39 GMT+0200 (CEST)"},
    {"inValue":20.72,"outValue":20.00,"date":"Sun Mar 19 2017 17:10:39 GMT+0200 (CEST)"},
    {"inValue":20.72,"outValue":20.00,"date":"Wed Apr 19 2017 17:10:39 GMT+0200 (CEST)"},

    {"inValue":20.72,"outValue":24.72,"date":"Wed May 10 2017 17:10:39 GMT+0200 (CEST)"},
    {"inValue":20.22,"outValue":23.00,"date":"Thu May 11 2017 17:10:39 GMT+0200 (CEST)"},
    {"inValue":18.72,"outValue":20.00,"date":"Fri May 12 2017 17:10:39 GMT+0200 (CEST)"},
    {"inValue":23.72,"outValue":25.00,"date":"Sat May 13 2017 17:10:39 GMT+0200 (CEST)"},
    {"inValue":19.66,"outValue":20.00,"date":"Sun May 14 2017 17:10:39 GMT+0200 (CEST)"},
    {"inValue":21.72,"outValue":28.15,"date":"Mon May 15 2017 17:10:39 GMT+0200 (CEST)"},
    {"inValue":20.72,"outValue":20.80,"date":"Tue May 16 2017 17:10:39 GMT+0200 (CEST)"},
    {"inValue":20.22,"outValue":23.00,"date":"Wed May 17 2017 17:10:39 GMT+0200 (CEST)"},
    {"inValue":22.82,"outValue":33.50,"date":"Thu May 18 2017 17:10:39 GMT+0200 (CEST)"},
    {"inValue":20.72,"outValue":24.72,"date":"Fri May 19 2017 17:10:39 GMT+0200 (CEST)"},
    {"inValue":20.22,"outValue":23.00,"date":"Sat May 20 2017 17:10:39 GMT+0200 (CEST)"},

    {"inValue":21.72,"outValue":28.15,"date":"Mon May 22 2017 17:10:39 GMT+0200 (CEST)"},
    {"inValue":20.72,"outValue":20.80,"date":"Tue May 23 2017 17:10:39 GMT+0200 (CEST)"},
    {"inValue":20.22,"outValue":23.00,"date":"Wed May 24 2017 17:10:39 GMT+0200 (CEST)"},
    {"inValue":22.82,"outValue":33.50,"date":"Thu May 25 2017 17:10:39 GMT+0200 (CEST)"},
    {"inValue":20.72,"outValue":24.72,"date":"Fri May 26 2017 17:10:39 GMT+0200 (CEST)"},
    {"inValue":20.22,"outValue":23.00,"date":"Sat May 27 2017 17:10:39 GMT+0200 (CEST)"},

    {"inValue":18.72,"outValue":20.00,"date":"Mon Jun 19 2017 17:10:39 GMT+0200 (CEST)"},
    {"inValue":23.72,"outValue":25.00,"date":"Wed Jul 19 2017 17:10:39 GMT+0200 (CEST)"},
    {"inValue":19.66,"outValue":20.00,"date":"Sat Aug 19 2017 17:10:39 GMT+0200 (CEST)"},
    {"inValue":21.72,"outValue":28.15,"date":"Mon Sep 18 2017 17:10:39 GMT+0200 (CEST)"},
    {"inValue":20.72,"outValue":20.80,"date":"Mon Oct 16 2017 17:10:39 GMT+0200 (CEST)"},
    {"inValue":20.22,"outValue":23.00,"date":"Sun Nov 19 2017 17:10:39 GMT+0200 (CEST)"},
    {"inValue":22.82,"outValue":15.50,"date":"Mon Dec 18 2017 17:10:39 GMT+0200 (CEST)"}
  ]};

  createPanel(users[0], data, address, function(panel){
    let date = new Date();
    date.setSeconds(date.getSeconds()+2);
    let dateEnd = new Date();
    dateEnd.setSeconds(date.getSeconds()+2);
    createEvent(panel,date, dateEnd);
  });

  address = 'localhost';
  data = { means : [
    {"inValue":20.72,"outValue":24.72,"date":"Fri May 19 2017 17:10:39 GMT+0200 (CEST)"},
    {"inValue":20.72,"outValue":23.00,"date":"Sat May 20 2017 17:10:39 GMT+0200 (CEST)"},
    {"inValue":20.72,"outValue":10.00,"date":"Fri Feb 19 2017 17:10:39 GMT+0200 (CEST)"},
    {"inValue":20.72,"outValue":20.00,"date":"Fri Feb 19 2017 17:10:39 GMT+0200 (CEST)"},
    {"inValue":20.72,"outValue":20.00,"date":"Fri Dec 19 2017 17:10:39 GMT+0200 (CEST)"}
  ]};

  createPanel(users[1], data, address);

});

app.models.panel.create([{},{}]);
}
