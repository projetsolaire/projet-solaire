import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StatChartComponent } from './stat-chart-component';


@NgModule({
  declarations: [
    StatChartComponent,
  ],
  imports: [
    IonicPageModule.forChild(StatChartComponent),
  ],
  exports: [
    StatChartComponent
  ]
})
export class StatChartComponentModule {}
