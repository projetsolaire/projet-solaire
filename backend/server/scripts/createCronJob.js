var CronJob = require('cron').CronJob;
var app = require('../server');
var logger = require('../logger');


module.exports = function(panelId, state, date){
  var PanelModel = app.models.panel;
  logger.info("Cronjob created.");
    new CronJob(date,function(){
      PanelModel.switch(panelId,state, function(err){
        if(err){
          return logger.error(JSON.stringify(err));
        }

        logger.info("***** <CRONJOB> ******\
        \n***** panel : "+panelId+"\
        \n***** panel : "+panelId+"\
        \n***** state : "+state+"\
        \n***** date : "+date+"\
        \n***** </CRONJOB> ******");
      });
    }, function(){
      //runs when job stops
    },
  true); //endCronJob
};
