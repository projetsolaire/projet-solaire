import {Injectable} from "@angular/core";
import {AuthProvider} from "../providers/auth-provider";
import config from "../config";
import {UserModel} from "../models/UserModel";
import {JwtHelper} from "angular2-jwt";

@Injectable()
export class AuthController {

  // jwtHelper: JwtHelper = new JwtHelper();

  constructor(private authProvider: AuthProvider) { }

  signup(user: { email: string, password: string, username: string}): Promise<any> {
    return new Promise( (resolve, reject) => {
      this.authProvider.signup(user.email, user.password, user.username).subscribe(
        (res) => {
          resolve({message: 'success'})
        },
        (error) => reject(error)
      );
    })
  }

  login(usercreds: { email: string, password: string }): Promise<any> {
    return new Promise( (resolve, reject) => {
      this.authProvider.login(usercreds.email, usercreds.password).subscribe(
        (res) => {
          this.storeUser(JSON.stringify(res));
          resolve({message: 'success'})
        },
        (error) => {
          reject(error.error);
        }
      );
    })
  }

  storeUser(user: string) {
    window.localStorage.setItem(config.userStore, user);
  }

  isLogin(): boolean {
    return window.localStorage.getItem(config.userStore) ? true : false;
  }

  getAuthInfo(): any {
    const userStore = window.localStorage.getItem(config.userStore);
    return userStore ? JSON.parse(userStore) : null;
  }

  getUser(): UserModel {
    const info = this.getAuthInfo();
    const user = new UserModel(info.userId, info.email, info.username);
    return user;
  }

  logout() {
    // TODO request logout ?
    window.localStorage.clear();
  }
}
