import { isDevMode } from '@angular/core';

// const API_HOST = 'http://192.168.1.46:3000';
const API_HOST_LOCAL = 'http://localhost:3000';
const API_HOST_DIST= 'http://192.168.1.46:3000';
//const API_HOST_DIST= 'http://176.31.96.30:3000';
const API_HOST_PROD= 'http://176.31.96.30:3000';

const API_HOST = isDevMode() ? ( window['cordova'] ? API_HOST_DIST : API_HOST_LOCAL) : API_HOST_PROD;

export default {
  host: API_HOST,
  apiUrl: API_HOST + '/api',
  userStore: 'user_store'
};
