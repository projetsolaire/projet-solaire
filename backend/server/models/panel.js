  'use strict';
const request = require("request");
var logger = require('../logger');
var isAssociated = require('../scripts/isAssociated');

function setState(panel,state, cb){
  panel.updateAttribute("state", state, function(err, p){
    if (err)  return cb(err);
    if(state != p.state) return cb(500);

    return cb(null, p.state, p.id, p.userId);
    logger.info(p);
  });
};

function setSpeed(panel, speed, cb){
  panel.updateAttribute("speed", speed, function(err, p){
    if (err)  return cb(err);
    if(speed != p.speed) return cb(500);

    return cb(null, p.speed, p.id, p.userId);
    logger.info(p);
  });
};

function setUserId(panel, userId, cb){
  panel.updateAttribute("userId", userId, function(err, p){
    if (err)  return cb(err);
    if(userId != p.userId) return cb(500);

    return cb(null, p.states, p.id, p.userId);
  });
};

function setRemoteAddress(panel, address, cb){
  panel.updateAttribute("address", address, function(err, p){
    if (err)  return cb(err);
    if(address != p.address) return cb(500);

    return cb(null, p.id, p.address);
  });
};

function setName(panel, name, cb){
  panel.updateAttribute("name", name, function(err, p){
    if (err)  return cb(err);
    if(name != p.name) return cb(500);

    return cb(null, p.states, p.id, p.name);
  });
};

module.exports = function(Panel) {

  Panel.observe('before save',function(ctx, next) {
    if(ctx.instance && !ctx.instance.data){
      ctx.instance.data = { means : [] };
    }
    next();
  });

  Panel.switch = function(id, state, cb){
    Panel.findById(id, function (err1, panel) {
      if( err1) return cb(err1);

      if(!isAssociated(panel)){
        //return setState(panel, state, cb);
        return cb({"statusCode" : 400, "msg" : "The panel has not been assiocated."})
      }

      request.patch({
        url: 'http://'+panel.address+'/'+state
      }, function (err2, resp, body){
        if (err2)  return cb(err2);

        state = JSON.parse(body).state;
        return setState(panel,state,cb);
      });
    });
  };

  Panel.remoteMethod(
    'switch', {
      http: {
        path: '/:id/switch/:state',
        verb: 'patch'
      },
      returns: [
        {
          arg: 'state',
          type: 'string'
        },
        {
          arg: 'id',
          type: 'string'
        },
        {
          arg: 'userId',
          type: 'string'
        }
      ],
      accepts: [{arg: 'id', type: 'string', required: true}, {arg: 'state', type: 'string', required: true}],
      description: "Switch off/on the panel"
    });

    Panel.speed = function(id, speed, cb){
      Panel.findById(id, function (err1, panel) {
        if( err1) return cb(err1);

        if(!isAssociated(panel)){
          //return setSpeed(panel, speed, cb);
          return cb({"statusCode" : 400, "msg" : "The panel has not been assiocated."})
        }

        request.patch({
          url: 'http://'+panel.address+'/speed?speed='+speed
        }, function (err2, resp, body){
          if (err2)  return cb(err2);

          speed = JSON.parse(body).speed;
          logger.info(speed);
          return setSpeed(panel,speed,cb);
        });
      });
    };

    Panel.remoteMethod(
      'speed', {
        http: {
          path: '/:id/speed',
          verb: 'patch'
        },
        returns: [
          {
            arg: 'speed',
            type: 'string'
          },
          {
            arg: 'id',
            type: 'string'
          },
          {
            arg: 'userId',
            type: 'string'
          }
        ],
        accepts: [{arg: 'id', type: 'string', required: true}, {arg: 'speed', type: 'number', required: true}],
        description: "Choose the speed of the panel"
      });

      Panel.associate = function(id, userId, cb){
        Panel.findById(id, function (err1, panel) {
          if( err1) return cb(err1);
          if(panel.userId) return cb({"statusCode": 409, "message": "already assiocated"})
          return setUserId(panel, userId, cb);
        });
      };

      Panel.remoteMethod(
        'associate', {
          http: {
            path: '/:id/associate',
            verb: 'patch'
          },
          returns: [
            {
              arg: 'state',
              type: 'string'
            },
            {
              arg: 'id',
              type: 'string'
            },
            {
              arg: 'userId',
              type: 'string'
            }
          ],
          accepts: [{arg: 'id', type: 'string', required: true}, {arg: 'userId', type: 'string', required: true}],
          description: "Associate an user to a panel"
        });

        Panel.edit = function(id, name, cb){
          Panel.findById(id, function (err1, panel) {
            if( err1) return cb(err1);
            return setName(panel, name, cb);
          });
        };

        Panel.remoteMethod(
          'edit', {
            http: {
              path: '/:id/edit',
              verb: 'patch'
            },
            returns: [
              {
                arg: 'state',
                type: 'string'
              },
              {
                arg: 'id',
                type: 'string'
              },
              {
                arg: 'name',
                type: 'string'
              }
            ],
            accepts: [{arg: 'id', type: 'string', required: true}, {arg: 'name', type: 'string', required: true}],
            description: "Edit the name of a panel"
          });


        Panel.data = function(id, meanIn, meanOut, cb){
          Panel.findById(id, function (err1, panel) {
            if( err1) return cb(err1);
            panel.data.means.push({outValue : meanOut, inValue : meanIn, date : new Date().toString()});

            panel.updateAttribute("data", panel.data, function(err2, p){
              if (err2)  return cb(err2);
              logger.info("lastValueRegistered : "+JSON.stringify(p.data.means[p.data.means.length-1]));

              return cb(null, p.state, p.id, p.userId, p.data);
            });
          });
        };

        Panel.remoteMethod(
          'data', {
            http: {
              path: '/:id/data',
              verb: 'post'
            },
            returns: [
              {
                arg: 'state',
                type: 'string'
              },
              {
                arg: 'id',
                type: 'string'
              },
              {
                arg: 'userId',
                type: 'string'
              },
              {
                arg: 'data',
                type: 'object'
              }
            ],
            accepts: [{arg : 'id', type : 'number', required : true},
            {arg: 'meanIn', type: 'number', required: true},
            {arg: 'meanOut', type: 'number', required: true}
          ],
          description: "Send new data to the panel"
        });

        Panel.dataForYear = function(id, year, cb){
          Panel.findById(id, function (err1, panel) {
            if( err1) return cb(err1);
            let means = panel.data.means;
            let result = {"in": [], "out": []};
            let counters = {};
            means.forEach(function(mean) {
              let date = new Date(mean.date);
              if(date.getFullYear() == year){
                if(!counters[date.getMonth()]) counters[date.getMonth()] = 1;
                else counters[date.getMonth()]++;
                if(!result.in[date.getMonth()]) result.in[date.getMonth()] = 0;
                if(!result.out[date.getMonth()]) result.out[date.getMonth()] = 0;
                result.out[date.getMonth()] = result.out[date.getMonth()] + mean.outValue;
                result.in[date.getMonth()] = result.in[date.getMonth()] + mean.inValue;
              }
            });
            for(let i = 0; i < 12; i++){
              result.in[i] = result.in[i] / counters[i];
              result.out[i] = result.out[i] / counters[i];
            }
            return cb(null, panel.state, panel.id, panel.userId, result);
          });
        };

        Panel.remoteMethod(
          'dataForYear', {
            http: {
              path: '/:id/data/year/:year',
              verb: 'get'
            },
            returns: [
              {
                arg: 'state',
                type: 'string'
              },
              {
                arg: 'id',
                type: 'string'
              },
              {
                arg: 'userId',
                type: 'string'
              },
              {
                arg: 'data',
                type: 'array'
              }
            ],
            accepts: [{arg : 'id', type : 'number', required : true}, {arg : 'year', type : 'number', required : true}],
            description: "Get data for one year"
          });

          Panel.dataForMonth = function(id, year, month, cb){
            Panel.findById(id, function (err1, panel) {
              if( err1) return cb(err1);
              let means = panel.data.means;
              let result = {"in":[], "out": []};
              let counters = {};
              means.forEach(function(mean) {
                let date = new Date(mean.date);
                if(date.getFullYear() == year){
                  if(date.getMonth() == month){
                    if(!counters[date.getDate()-1]) counters[date.getDate()-1] = 1;
                    else counters[date.getDate()-1]++;
                    if(!result.in[date.getDate()-1]) result.in[date.getDate()-1] = 0;
                    if(!result.out[date.getDate()-1]) result.out[date.getDate()-1] = 0;
                    result.in[date.getDate()-1] = result.in[date.getDate()-1] + mean.inValue;
                    result.out[date.getDate()-1] = result.out[date.getDate()-1] + mean.outValue;
                  }
                }
              });
              for(let i = 1; i < 32; i++){
                result.in[i] = result.in[i] / counters[i];
                result.out[i] = result.out[i] / counters[i];
              }
              return cb(null, panel.state, panel.id, panel.userId, result);
            });
          };

          Panel.remoteMethod(
            'dataForMonth', {
              http: {
                path: '/:id/data/month/:year/:month',
                verb: 'get'
              },
              returns: [
                {
                  arg: 'state',
                  type: 'string'
                },
                {
                  arg: 'id',
                  type: 'string'
                },
                {
                  arg: 'userId',
                  type: 'string'
                },
                {
                  arg: 'data',
                  type: 'array'
                }
              ],
              accepts: [{arg : 'id', type : 'number', required : true}, {arg : 'year', type : 'number', required : true}, {arg : 'month', type : 'number', required : true}],
              description: "Get data for one month"
            });

            Panel.dataForDay = function(id, year, month, day, cb){
              Panel.findById(id, function (err1, panel) {
                if( err1) return cb(err1);
                let means = panel.data.means;
                let result = {"in":[], "out":[]};
                let counters = {};
                means.forEach(function(mean) {
                  let date = new Date(mean.date);
                  if(date.getFullYear() == year){
                    if(date.getMonth() == month){
                      if (date.getDate() == day){
                        if(!counters[date.getHours()]) counters[date.getHours()] = 1;
                        else counters[date.getHours()]++;
                        if(!result.in[date.getHours()]) result.in[date.getHours()] = 0;
                        if(!result.out[date.getHours()]) result.out[date.getHours()] = 0;
                        result.in[date.getHours()] = result.in[date.getHours()] + mean.inValue;
                        result.out[date.getHours()] = result.out[date.getHours()] + mean.outValue;
                      }
                    }
                  }
                });
                for(let i = 0; i < 24; i++){
                  result.in[i] = result.in[i] / counters[i];
                  result.out[i] = result.out[i] / counters[i];
                }
                return cb(null, panel.state, panel.id, panel.userId, result);
              });
            };

            Panel.remoteMethod(
              'dataForDay', {
                http: {
                  path: '/:id/data/day/:year/:month/:day',
                  verb: 'get'
                },
                returns: [
                  {
                    arg: 'state',
                    type: 'string'
                  },
                  {
                    arg: 'id',
                    type: 'string'
                  },
                  {
                    arg: 'userId',
                    type: 'string'
                  },
                  {
                    arg: 'data',
                    type: 'array'
                  }
                ],
                accepts: [{arg : 'id', type : 'number', required : true}, {arg : 'year', type : 'number', required : true}, {arg : 'month', type : 'number', required : true}, {arg : 'day', type : 'number', required : true}],
                description: "Get data for one day"
              });

      Panel.subscribe = function(id, req, cb){
        Panel.findById(id, function(err, panel){
          if(err) return cb(err);
          let ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
          console.log(ip);
          ip += ':8080';
          setRemoteAddress(panel, ip, cb);

        });
      };

      Panel.remoteMethod(
        'subscribe', {
          http : {
            path : '/:id/subscribe',
            verb : 'patch'
          },
          returns : [
            {
              arg : 'id',
              type : 'number'
            },
            {
              arg : 'ip',
              type : 'string'
            }
          ],
          accepts : [
            {
              arg : 'id', type : 'number', required : true
            },
            {
              arg : 'req', type : 'object', required : true,
              http : {source : 'req'}
            }
          ]
        });
};
