import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SolarPanelDetailsPage } from './solar-panel-details';

@NgModule({
  declarations: [
    SolarPanelDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(SolarPanelDetailsPage),
  ],
  exports: [
    SolarPanelDetailsPage
  ]
})
export class SolarPanelDetailsModule {}
