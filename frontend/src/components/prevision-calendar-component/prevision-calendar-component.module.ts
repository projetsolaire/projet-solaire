import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PrevisionCalendarComponent } from './prevision-calendar-component';

@NgModule({
  declarations: [
    PrevisionCalendarComponent,
  ],
  imports: [
    IonicPageModule.forChild(PrevisionCalendarComponent),
  ],
  exports: [
    PrevisionCalendarComponent
  ]
})
export class PrevisionCalendarComponentModule {}
