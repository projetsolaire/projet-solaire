
export class UserModel {

  constructor(public id: string,
              public email: string,
              public name: string = null,
              public age: number = null
  ) { }

}
