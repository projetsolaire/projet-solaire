import {Component, Input, OnInit} from '@angular/core';
import {SolarPanelModel} from "../../models/SolarPanelModel";
import {SolarPanelProvider} from "../../providers/solar-panel-provider";

/**
 * Generated class for the StatChartComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'stat-chart-component',
  templateUrl: 'stat-chart-component.html'
})
export class StatChartComponent implements OnInit {
  @Input() panel: SolarPanelModel;

  constructor(private panelProvider: SolarPanelProvider) {
  }

  ngOnInit() {
    this.panelProvider.getStatsYear(this.panel.id, this.currentYear.toString()).subscribe(
      (res) => {console.log(res);
        this.lineChartData = [{data: res.data.in, label: "Temperature d'entrée"},{data: res.data.out, label: "Temperature de sortie"}];
      }
    );
  }

  currentYear: number = new Date(Date.now()).getFullYear();
  currentMonth: number = 1;
  currentDay: string = "";
  maxDay: string = this.currentYear + "-" + (( (new Date(Date.now()).getMonth() + 1).toString().length==1) ? "0" + (new Date(Date.now()).getMonth() + 1) : (new Date(Date.now()).getMonth() + 1) ) + "-" + new Date(Date.now()).getDate();
  //button activate
  activateChart: string = "year";
  // lineChart
  lineChartData: Array<any> = [
    {data: null, label: "Temperature d'entrée"},
     {data: null, label: "Temperature de sortie"}
  ];
  lineChartLabelsYear: Array<any> = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'];
  lineChartLabelsMonth: Array<any> = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31'];
  lineChartLabelsDay: Array<any> = ['00h', '01h', '02h', '03h', '04h', '05h', '06h', '07h', '08h', '09h', '10h', '11h', '12h', '13h', '14h', '15h', '16h', '17h', '18h', '19h', '20h', '21h', '22h', '23h'];
  currentChart: Array<any> = this.lineChartLabelsYear;
  lineChartOptions: any = {
    responsive: true
  };
  lineChartColors: Array<any> = [
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];

  // events
  chartClicked(e: any): void {
    console.log(e);
  }

  chartHovered(e: any): void {
    console.log(e);
  }

  changeCurrentChart(chart: any, timer: string) {
    this.currentChart = chart;
    this.activateChart = timer;
    if (timer == "year") {
      this.panelProvider.getStatsYear(this.panel.id,this.currentYear.toString()).subscribe(
       (res) => { console.log(res);
       this.lineChartData = [{data: res.data.in, label:"Temperature d'entrée"},{data: res.data.out, label: "Temperature de sortie"}];
       }
       );
    } else if (timer == "month") {
      this.panelProvider.getStatsMonth(this.panel.id, this.currentYear.toString(), (this.currentMonth - 1).toString()).subscribe(
        (res) => {
          this.lineChartData = [{data: res.data.in, label: "Temperature d'entrée"},{data: res.data.out, label: "Temperature de sortie"}];
        }
      );
      this.currentMonth=1;
    } else if (timer == "day") {
      this.panelProvider.getStatsDay(this.panel.id, this.currentYear.toString(), (this.currentMonth - 1).toString(), (this.currentDay=="") ? new Date(Date.now()).getDate().toString() : this.currentDay.slice(-2)).subscribe(
        (res) => {
          this.lineChartData = [{data: res.data.in, label: "Temperature d'entrée"},{data: res.data.out, label: "Temperature de sortie"}];
        }
      );
      this.currentDay="";
    }
  }

  isActive(time: string) {
    if (time == this.activateChart) {
      return true;
    }
  }

  onChange(val:any) {
   switch (val){
   case "Janvier": {this.currentMonth=1; break}
   case "Février": {this.currentMonth=2; break}
   case "Mars": {this.currentMonth=3; break}
   case "Avril": {this.currentMonth=4;break;}
   case "Mai": {this.currentMonth=5;break}
   case "Juin": {this.currentMonth=6;break}
   case "Juillet": {this.currentMonth=7; break}
   case "Août": {this.currentMonth=8; break}
   case "Septembre": {this.currentMonth=9; break}
   case "Octobre": {this.currentMonth=10; break}
   case "Novembre": {this.currentMonth=11; break}
   case "Décembre": {this.currentMonth=12; break}
   }
  }
}
