var createCronJob = require('../scripts/createCronJob');

/*
* Launched in create-sample-data after panels are created
*/

module.exports = function(app){

  var Event = app.models.event;
  var Panel = app.models.panel;

  Event.find({},function(err,events){
    if(err) throw err;

    events.forEach(
      (e) => {
        Panel.findOne({id : e.panelId}, function(err, panel){
          if(err) throw err;

          createCronJob(e.panelId,e.state,e.start);

        });//endFindOne

      }
    );//endForEach
  });
};
