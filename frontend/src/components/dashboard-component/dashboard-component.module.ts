import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DashboardComponent } from './dashboard-component';

@NgModule({
  declarations: [
    DashboardComponent,
  ],
  imports: [
    IonicPageModule.forChild(DashboardComponent),
  ],
  exports: [
    DashboardComponent
  ]
})
export class DashboardComponentModule {}
