import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import {BasicProvider} from "./basic-provider";
import {Observable} from "rxjs/Observable";
import CryptoJS from 'crypto-js'

/*
  Generated class for the AuthProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class AuthProvider extends BasicProvider {

  constructor(public http: Http) { super(); }

  login(email: string, password: string): Observable<any> {
    const params = { email: email, password: CryptoJS.SHA256(password).toString() };
    // const params = { email: email, password: password };
    return this.http.post(this.apiUrl + '/users/processLogin', params)
                    .map(this.extractData)
                    .catch(this.handleError);
  }

  signup(email: string, password: string, username: string = email): Observable<any> {
    const params = { email: email, password: CryptoJS.SHA256(password).toString(), username: username };

    return this.http.post(this.apiUrl + '/users', params)
                    .map(this.extractData)
                    .catch(this.handleError);
  }

}
