'use strict';

var createCronJob = require('../scripts/createCronJob');
var app = require('../server');
var isAssociated = require('../scripts/isAssociated');

module.exports = function(Event) {


  Event.observe('before save',function(ctx, next) {
    var PanelModel = app.models.panel;

    if(ctx.instance){
      PanelModel.findById(ctx.instance.panelId, function(err, panel){
        if(err){
          return next(err);
        }

        if(!isAssociated(panel)){
           err = new Error("The panel has not been associated.")
           err.statusCode = 400;
            return next(err);
        }
        ctx.instance.start = new Date(ctx.instance.start);
        if(ctx.instance.end){
            ctx.instance.end = new Date(ctx.instance.end);
        }

        next();
      });
    }
  });

  Event.observe('after save', function(ctx,next){
    let evt = ctx.instance;
    if(evt){
      createCronJob(evt.panelId, evt.state, evt.start);

      if(evt.end && (evt.end.getTime() > evt.start.getTime())){
        if(evt.state == "on"){
          createCronJob(evt.panelId, "off", evt.end);
        }else {
          createCronJob(evt.panelId, "on", evt.end);
        }
      }
    }
    next();
  });
};
