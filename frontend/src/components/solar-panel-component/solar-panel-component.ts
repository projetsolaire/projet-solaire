import {Component, Input} from '@angular/core';
import {SolarPanelModel} from "../../models/SolarPanelModel";
import {AlertController, NavController, ToastController} from "ionic-angular";
import {SolarPanelDetailsPage} from "../../pages/solar-panel-details/solar-panel-details";
import {SolarPanelProvider} from "../../providers/solar-panel-provider";

@Component({
  selector: 'solar-panel-component',
  templateUrl: 'solar-panel-component.html'
})
export class SolarPanelComponent {

  @Input() solarPanel: SolarPanelModel;

  constructor(public navCtrl: NavController,
              public toastCtrl: ToastController,
              public panelProvider: SolarPanelProvider,
              private alertCtrl: AlertController
  ) { }

  tapSolarPanel() {
    this.navCtrl.push(SolarPanelDetailsPage, { solarPanel: this.solarPanel });
  }

  presentRenamePrompt() {
    const self = this;
    let alert = this.alertCtrl.create({
      title: 'Edit the panel name',
      inputs: [
        {
          name: 'name',
          placeholder: 'new name'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Save',
          handler: data => {
            self.rename(data.name);
          }
        }
      ]
    });
    alert.present();
  }

  rename(newName: string) {
    this.panelProvider.rename(this.solarPanel.id, newName).subscribe(
      (res) => {
        this.solarPanel.name = res.name;
      },
      (err) => {
        console.log(err);
      }
    )
  }

}
