import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SignInComponent } from './sign-in-component';

@NgModule({
  declarations: [
    SignInComponent,
  ],
  imports: [
    IonicPageModule.forChild(SignInComponent),
  ],
  exports: [
    SignInComponent
  ]
})
export class SignInComponentModule {}
