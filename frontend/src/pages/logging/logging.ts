import {Component} from "@angular/core";
import {IonicPage, NavController, NavParams} from "ionic-angular";
import {AuthProvider} from "../../providers/auth-provider";
import {AuthController} from "../../controllers/auth-controller";

/**
 * Generated class for the Logging page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-logging',
  templateUrl: 'logging.html',
})
export class LoggingPage {

  form: string = 'signin';

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private authProvider: AuthProvider,
              private authCtrl: AuthController
  ) { }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Logging');
  }

}
