'use strict';

module.exports = function(user) {

user.observe('before save',function(ctx, next) {
    ctx.instance.realm = "";
    next();
  });


  user.processLogin = function(credentials, include, cb) {

    this.login(credentials, include, function(err, resp) {
      if (err) return cb(err);
      user.findById(resp.userId, function (err1, u) {
        if( err1) return cb(err1);
        let authInfo = {
          id: resp.id,
          username: u.username,
          email: u.email,
          userId: u.id
        };
        cb(err, authInfo);
      });
    });
  };
  user.remoteMethod(
    'processLogin',
    {
      description: 'Login method with Role data information embedded in return',
      accepts: [
        {arg: 'credentials', type: 'object', required: true, http: {source: 'body'}},
        {arg: 'include', type: ['string'], http: {source: 'query' },
        description: 'Related objects to include in the response. ' +
        'See the description of return value for more details.'}
      ],
      returns: {
        arg: 'accessToken', type: 'object', root: true,
        description: 'User Model'
      },
      http: {verb: 'post'}
    }
  );
};
