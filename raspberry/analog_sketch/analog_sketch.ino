#include <math.h>

int COUNTER_FORCE_PUSH = 200;    // espacement où l'on force le push
int COUNTER_PUSH_INIT = 1; 
const int B = 4275;               // B value of the thermistor
const int R0 = 100000;            // R0 = 100k
const int pinTempSensor1 = A5;     // Grove - Temperature Sensor connect to A5 : CAPTEUR ENTREE
const int pinTempSensor2 = A3;     // Grove - Temperature Sensor connect to A3 : CAPTEUR SORTIE
int isFetching = 0;
float R = 0.0;
byte byteRead = 0;
int vitesse = 0;


void setup()
{
  Serial.begin(9600);
  pinMode(10, OUTPUT);
}

void loop()
{
	
  if(isFetching == 0) {
	digitalWrite(10, LOW);
  }
  
  //on fetch et on push que si raspberry nous le dit
  if (isFetching == 1) {

    //On allume une led sur port 10 pour debugguer
    digitalWrite(10, HIGH);

    //On calcul temperature 1
    int value1 = analogRead(pinTempSensor1);
    R = 1023.0 / ((float)value1) - 1.0;
    R = 100000.0 * R;
    float temperature1 = 1.0 / (log(R / 100000.0) / B + 1 / 298.15) - 273.15; //convert to temperature via datasheet

    //On calcul temperature 1
    int value2 = analogRead(pinTempSensor2);
    R = 1023.0 / ((float)value2) - 1.0;
    R = 100000.0 * R;
    float temperature2 = 1.0 / (log(R / 100000.0) / B + 1 / 298.15) - 273.15; //convert to temperature via datasheet

    //on force le push au début
    if (COUNTER_PUSH_INIT) {
      Serial.print(temperature1);
      Serial.print(";");
      Serial.println(temperature2);
       
      COUNTER_PUSH_INIT = 0;
    }

    delay(100);

    if ( COUNTER_FORCE_PUSH == 0 ) {
      Serial.print(temperature1);
      Serial.print(";");
      Serial.println(temperature2);
      COUNTER_FORCE_PUSH = 200;
    }

    COUNTER_FORCE_PUSH--;

  }


  
  //On écoute ordre raspberry
  if (Serial.available() > 0) {
    //read the incoming byte:
    byteRead = Serial.read();
    
    if (byteRead == 101) {
      isFetching = 0;
    } else if ( byteRead == 126) {
      isFetching = 1;
    } else if ( byteRead >= 0 && byteRead <= 100 ) {
       analogWrite(9, map(byteRead, 0, 100, 0, 255));
    } else {
      
    }    
    COUNTER_PUSH_INIT = isFetching;

  }

}
