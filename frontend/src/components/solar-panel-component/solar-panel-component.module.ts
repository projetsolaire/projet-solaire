import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SolarPanelComponent } from './solar-panel-component';

@NgModule({
  declarations: [
    SolarPanelComponent,
  ],
  imports: [
    IonicPageModule.forChild(SolarPanelComponent),
  ],
  exports: [
    SolarPanelComponent
  ]
})
export class SolarPanelComponentModule {}
